params.output_dir = false
//seqsero
process SEQSERO2 {
   memory '2 GB'
   publishDir "${params.output_dir}",
   mode:'copy',
   saveAs: { file -> "seqsero2_output/${fasta_file}.tsv"}
 
  input:
  file(fasta_file)

  output:
  file('seqsero2_output/*.tsv') 

  """
  SeqSero2_package.py -m k -p 2 -t 4 -i ${fasta_file} -d seqsero2_output
  """
}

//Sistr_cmd
process SISTR_CMD {
   memory '2 GB'
   publishDir "${params.output_dir}",
   mode:'copy',
   saveAs: { file -> "sistr_output/${fasta_file}.csv"}
 
  input:
  file (fasta_file)

  output:
  file ('sistr_output/results.csv') 

"""
mkdir sistr_output
sistr --qc -vv --alleles-output sistr_output/allele-results.json --novel-alleles sistr_output/novel-alleles.fasta --cgmlst-profiles sistr_output/cgmlst-profiles.csv -f csv -o sistr_output/results.csv ${fasta_file}

"""
}

//ECtyper
process ECTYPER {
   memory '2 GB'
   publishDir "${params.output_dir}",
   mode:'copy',
   saveAs: { file -> "ectyper_output/${fasta_file}.tsv"}
 
  input:
  file (fasta_file)

  output:
  file ('ectyper_output/*.tsv') 

  """
  ectyper -i ${fasta_file} -o ectyper_output

  """
}

//Ng-master
process NGMASTER {
   memory '2 GB'
   publishDir "${params.output_dir}",
   mode:'copy'
  
  input:
  file (fasta_file)

  output:
  file ('ngmaster_output/*.csv') 
  file ('ngmaster_output/*.fasta')
"""
  mkdir ngmaster_output
  ngmaster --csv ${fasta_file} > ngmaster_output/${fasta_file}.csv --printseq ngmaster_output/${fasta_file}_alleles.fasta 
 
"""
}
