nextflow.enable.dsl=2
params.input_dir = false
params.output_dir = false
params.species = false

include { SEQSERO2; SISTR_CMD; ECTYPER; NGMASTER } from './processes/processes'

workflow {
if (params.input_dir && params.output_dir) {
  fastas = Channel
    .fromPath(params.input_dir)
    .ifEmpty { error "Cannot find any fastas matching: ${fasta_file}" }

if (params.species=="Salmonella"){
 SEQSERO2(fastas)
 SISTR_CMD(fastas)
}
else {
 if (params.species=="Ecoli"){
  ECTYPER(fastas)
  }
  else {
   if (params.species=="Ngono") {
    NGMASTER(fastas)
    }
  }
}
}
}