##Please to run change path to nextflow script and data base directories: lines 2 and 3  
NEXTFLOW_PIPELINE_DIR=${PWD}
DATA_DIR=${PWD}/salmonella
SPECIES=Salmonella

nextflow run ${NEXTFLOW_PIPELINE_DIR}/main.nf \
    --input_dir "${DATA_DIR}/fastas/*.fasta" \
    --output_dir ${DATA_DIR}/MT_output \
    --species ${SPECIES} \
    -w ${DATA_DIR}/MT_output/work \
    -resume \
    -profile docker 
