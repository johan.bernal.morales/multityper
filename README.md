# __**MultiTyper v0.3**__

This process packaged together typing software for different species include Salmonella spp., Escherichia coli, Enterococcus sp.(*), Neisseria gonorrhoeae, Streptococcus pneumoniae(*) with scalable workflow using nextflow manager and build on docker container.

### Software per specie

Salmonella: Seqsero2 v1.1.1; Sistr_cmd v1.1.1 <br>
Ecoli: Ectyper v1.0.0 <br>
Enterococcus: CHEWBBAKKA <br>
Neisseria gonorrhoeae: Ng-master v0.5.6<br>
Streptococcus pneumoniae: Pneumocat, Seroba <br>

*not available in this version - comming soon!!

### Pre-requisites

* Nextflow
* Docker

### Docker image

```
docker pull registry.gitlab.com/johan.bernal.morales/multityper:latest
```

### If it is requiered to test on your own data, edit path to data base directories, and define specie into the run file **"run_mt_test.sh"** 

_DATA_DIR=/path/to/data_base_directory_ <br>
_SPECIES= Salmonella or Ecoli or Ngono_ <br>

### run **MT**

```
bash run_mt_test.sh
````
### All fasta sequences used as examples for MT were downloaded from refseq NCBI ( https://www.ncbi.nlm.nih.gov/ )

### **All in one site!!**
